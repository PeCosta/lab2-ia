import sys
import logging
from util import evaluate, RandomTest
from copy import deepcopy

def nextBest(S,A):
	"""
	CLIMB THE HILL!!!
	Inputs:
		S-> Sentence with Clauses
		A-> Privious solution to climb from
	"""
	countF = -1
	sol = []

	#Iterate all varaibles from the solution
	for i in range(0,len(A)):

		#B needs a deepcopy!! B = A just does thing<-B<-A!!
		B = deepcopy(A)

		#Flip the variable
		val = A[i]
		B[i] = not val

		#evaluate new Solution!
		finish,count = evaluate(S,B)

		#Is it better?
		if(count > countF):
			countF = count
			sol = i

	#I know the variable to flip! R: THEN FLIP IT!
	A[sol] = not A[sol]
	return A

def GSAT(S,max_restarts,max_climbs,n):
	"""
		Implements GSAT Algorithm
		Inputs:
		S -> Sentence with Clauses (len(S) represents number of clauses)
		max_restart -> Maximun number of permited resets 
		max_climbs -> Maximun number of permited climbs 
	"""
	# Iterate until maximum restart reached
	for i in range(0,max_restarts):

		#Log INFO (You can chech this by placing the program on info or lower loggign state)
		logging.info("\nRestart Number ->"+str(i))
		
		#Geneate a random solution
		A = RandomTest(n)

		#Climb a max_climbs number of times
		for j in range(0,max_climbs):

			#Evaluate Possible Solution
			finish,count = evaluate(S,A)

			#Is the problem Solved?
			if(finish == True):
				#You got yourself a solution, that a boy
				return A , True, (i*j)
			#Guess not (Better luck next time)
			else:
				#New Climb
				A = nextBest(S,A)

				#Log INFO (You can chech this by placing the program on info or lower loggign state)
				log ="\nNext Calculated A->\n"+str(A)+"\n"
				logging.info(log)
	
	#No solution found :(	
	return [],False, (i*j)


#Unit Test
#def unitTestGSAT():
#	max_restarts = 15
#	max_climbs = 15
#
#	logging.basicConfig(stream=sys.stderr, level=logging.INFO, format = '%(asctime)-15s %(message)s')
#	Type,n,N,S = parser("Tests/uf20-01.cnf")
#	Solution,Valid  = GSAT(S,max_restarts,max_climbs,n)
#	if(Valid):
#		s "Solution->",Solution
#	else:
#		print "No Solution"
#	return

