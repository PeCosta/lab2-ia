import logging
import os
import csv

def parseIN(file):
	"""
	File parser
	Inputs:
		file -> File to be parsed
	Outputs:
		Type -> Tyoe of problem
		SymbolN -> Number of Variables 
		ClauseN -> Number of Clauses
		Sentance -> Sentance composed by the Clauses
	"""
	FILE = open(file)
	Sentance = []

	#Read Line by Line
	for line in FILE:
		#Comment
		if line[0] == 'c':
			pass
		#Parameters	
		elif line[0] == 'p':
			params = line.split()
			Type = params[1]
			SymbolN = int(params[2])
			ClauseN = int(params[3])
		
		#Crap
		elif line[0]=='%' or line[0]=='0' or line[0] == '\n':
			pass

		#Variables
		else:
			params = line.split()
			clause = []
			for param in params:
				if(param[0]!='0'):
					clause.append(int(param))
			Sentance.append(clause)

	FILE.close()
	#Log into debug Level
	Logg = "\nType->"+str(Type)+\
	"\nNumber os Symbols->"+str(SymbolN)+\
	"\nNumber of Clauses->"+str(ClauseN)+\
	"\nSentance->" + str(Sentance)+"\n"
	logging.debug(Logg)
	
	return Type, SymbolN, ClauseN, Sentance


def outFile(S,A,SymbolN,ClauseN,Type,FileName,Alg="GSAT",CPUsec = 0,MEASURE1 = 0):

	Solution  = ""
	directory = 'Sol/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	if A == []:
		if Alg == "DPLL":
			Solution = "0"
		else:
			Solution = "-1"

	else:
		Solution = "1"

	OUTFILE = open(directory+FileName ,"w")

	OUTFILE.write("c THE SAT\n")
	OUTFILE.write("c Pedro Costa, Pedro Roque\n")
	OUTFILE.write("c 2015\n")
	OUTFILE.write("c\n")
	OUTFILE.write("c Solution for "+ FileName+"\n")
	OUTFILE.write("c -----------------------\n")

	OUTFILE.write("s "+ Type + " " +Solution + " "+ str(SymbolN) +" " + str(ClauseN) +"\n")
	OUTFILE.write("t "+ Type + " " +Solution + " "+ str(SymbolN) +" " \
		+ str(ClauseN) + " " +str(CPUsec) + " " + str(MEASURE1) + "\n")

	for i,a in enumerate(A):
		if a == False:
			OUTFILE.write("v "+str(-(i+1))+"\n")
		else:
			OUTFILE.write("v "+str((i+1))+"\n")

	OUTFILE.close()
	return

def dataTreatment(alg,Data):
	Solution  = ""
	directory = 'MatlabData/'
	if not os.path.exists(directory):
		os.makedirs(directory)
	if alg == []:
		print('No algorithm specified')
		return
	if Data == []:
		print('No given data.')
		return

	FileName=alg+'.csv'
	

	#with open(directory+FileName , "wb") as f:
	#	writer = csv.writer(f)
   	#	writer.writerow(Data)
   	#	f.write('\n')

	file = open(directory+FileName, "a+")
	csvWriter = csv.writer( file )
	csvWriter.writerow(Data)
	file.close()

	return
