import os
import sys
import subprocess
import ast
import glob
import csv 
from parser import dataTreatment

def main():

	filelist = glob.glob("MatlabData/*.csv")
	for f in filelist:
		os.remove(f)

	finalOut =[]
	for file in glob.glob("Tests/Small/UF20/*.cnf"):
		print("Testing File->" + str(file))
		p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, error = p.communicate()
		print(error)
		try:
			outD = ast.literal_eval(out)
			#print(outD)
			DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
			GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
			WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
			#print DPLL
			dataTreatment('DPLL-S',DPLL)
			dataTreatment('GSAT-S',GSAT)
			dataTreatment('WalkSAT-S',WalkSAT)
		except:
			print("----OH NO----")
			print("----I am a dumb machine----")
			pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF50/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-L',DPLL)
	# 		dataTreatment('GSAT-L',GSAT)
	# 		dataTreatment('WalkSAT-L',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF75/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF100/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF125/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF150/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF175/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF225/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# finalOut =[]
	# for file in glob.glob("Tests/Small/UF250/*.cnf"):
	# 	print("Testing File->" + str(file))
	# 	p = subprocess.Popen(["python" ,"Lab2.py", str(file) ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# 	out, error = p.communicate()
	# 	print(error)
	# 	try:
	# 		outD = ast.literal_eval(out)
	# 		#finalOut.append(outD)
	# 		#DPLL = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['DPLL'][0],outD['DPLL'][1]]
	# 		GSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['GSAT'][0],outD['GSAT'][1]]
	# 		WalkSAT = [outD['Symbols'],outD['Clauses'],outD['RatioCN'],outD['WalkSAT'][0],outD['WalkSAT'][1]]
	# 		#dataTreatment('DPLL-NS',DPLL)
	# 		dataTreatment('GSAT-NS',GSAT)
	# 		dataTreatment('WalkSAT-NS',WalkSAT)
	# 		print(outD)
	# 	except:
	# 		pass

	# 	#print out
if __name__ == '__main__':
	main()