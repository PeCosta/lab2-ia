import sys
import logging
from parser import parseIN, outFile 
import time
import argparse

from WalkSAT import WalkSat 
from GSAT import GSAT
from DPLL import DPLL ,transformOut


def main():
	parser = argparse.ArgumentParser(usage=__doc__)
	parser.add_argument("file", type=str, default=None, help="CNF file.")
	args = parser.parse_args()

	max_restarts = 15
	max_climbs = 15
	
	Filein = args.file

	logging.basicConfig(stream=sys.stderr, level=logging.ERROR, format = '%(funcName)2s - %(message)s')
	Type,n,N,S = parseIN(Filein)
	Symbols = list(range(1,n+1))
	logging.info("Sentance->" + str(S))
	ratioCN= (float(N)/float(n))

	Start_time = time.time()
	WalkSATSolution, WalkSATValid, WalkSATIterN = WalkSat(S,p=0.5,max_flips=1000,n=n)
	Walk_time = time.time()- Start_time

	Start_time = time.time()
	GSATSolution, GSATValid, GSATIterN = GSAT(S,max_restarts,max_climbs,n)
	GSAT_time = time.time()- Start_time

	Start_time = time.time()
	DPLLSolution, DPLLValid = DPLL(S,Symbols)
	DPLLSolution = transformOut(DPLLSolution,Symbols)
	DPLL_time = time.time()- Start_time

	if(WalkSATValid):
		logging.error("WalkSAT - Found a Solution in ->" + str(Walk_time) + " s")
	else:
		logging.error("WalkSAT - Failed to find Solution in ->" + str(Walk_time) +" s")

	
	if(GSATValid):
		logging.error("GSAT - Found a Solution in ->" + str(GSAT_time) + " s")
	else:
		logging.error("GSAT - Failed to find Solution in ->" + str(GSAT_time) +" s")

	if(DPLLValid):
		logging.error("DPLL - Found a Solution in ->" + str(DPLL_time) + " s")
	else:
		logging.error("DPLL - Failed to find Solution in ->" + str(DPLL_time) +" s")


	Filein.rfind("/")
	fileout = "WalkSat_" + Filein[Filein.rfind("/")+1:]
	outFile(S=S,A=WalkSATSolution,SymbolN=n,ClauseN=N,Type=Type,FileName=fileout,Alg="WalkSAT",CPUsec=Walk_time,MEASURE1=WalkSATIterN)

	fileout = "GSat_" + Filein[Filein.rfind("/")+1:]
	outFile(S=S,A=GSATSolution,SymbolN=n,ClauseN=N,Type=Type,FileName=fileout,Alg="GSAT",CPUsec=GSAT_time,MEASURE1=GSATIterN)

	fileout = "DPLL_" + Filein[Filein.rfind("/")+1:]
	outFile(S=S,A=DPLLSolution,SymbolN=n,ClauseN=N,Type=Type,FileName=fileout,Alg="DPLL",CPUsec=DPLL_time,MEASURE1=0)

	#out = {'WalkSAT':[Walk_time,ratioCN,WalkSATValid],'GSAT':[GSAT_time,ratioCN,GSATValid], 'DPLL':[0,ratioCN,0]}
	out = {'WalkSAT':[Walk_time,WalkSATValid],'GSAT':[GSAT_time,GSATValid], 'DPLL':[DPLL_time,DPLLValid], 'RatioCN':ratioCN, 'Symbols':n, 'Clauses':N}
	
	print(out)
	return out
if __name__ == '__main__':
	main()