# SAT Problem Solver

The following repository provides a python program to solve SAT problems


### Algorithms

The program provides implementation for the following SAT solver algorithms:

* GSAT
* WalkSAT
* DPLL

###Contributors

* Pedro Costa
* Pedro Roque

###Notes
* Version 0.5 -- Parser supports all 
* Version 0.4 -- WalkSat -> Working
* Version 0.3 -- GSAT -> Working
* Version 0.2 -- GSAT -> Implements random
* Version 0.1 --Parser Implemented

Written in python3.x