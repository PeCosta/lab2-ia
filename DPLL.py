import sys
import logging
import util
import copy


def hasPureSymbol(sentence,symbols,model):
	""" Check for Pure Symbols in a sentence.
		Inputs:
		-> sentence: a sentence in which to look for pure symbol.

		Returns:
		-> 'None' if no pure symbols are found;
		-> symbol and symbol position for first pure symbol found."""
	
	# Create empty symbol dictionary
	symbolList = {}
	
	# Runs through set of clauses in sentence
	for i,clause in enumerate(sentence):
		# Runs through symbols on a clause
	 	for j,sym in enumerate(clause):

	 		#Checks if variable exists in dictionary
	 		if not (abs(sym) in symbolList):
	 			# If doesn't exist, add it and set occurence to 1
	 			symbolList[abs(sym)]=[tuple([i,j]),1,sym]
 			else:
 				# If it exist's, increase occurance
 				symbolList[abs(sym)][1]+=1

 	# Runs through symbolList in sentence
	for index, val in enumerate(symbolList.values()):
		#Returns first symbol that appears only once 
		if val[1] == 1 and val[2] in symbols:
			sym,pos = list(symbolList.items())[index]
			if val[2] > 0:
				condition = True
			else:
				condition = False
			return [abs(val[2]),condition]

	return [None,None]

def hasUnitClause(sentence,symbols,model):
	""" Looks for unit clauses on a given sentence.
		Inputs:
		-> a sentence in which to look for a unit clause

		Returns:
		-> 'None' if no unit clauses are found;
		-> clause index and clause for first unit clause found."""
	# Initialize clause index
	cindex = 0
	
	# Runs through clauses in sentence
	for c in sentence:
		
		# returns a clause and it's position if it contains only one symbol
		if len(c) == 1 and c[0] in symbols:
			if c[0] > 0:
				condition = True
			else:
				condition = False

			return [abs(c[0]), condition]
		
		cindex=cindex+1

	return [None,None]

def simplifySentence(sentence,variable):
	""" Returns a simplified sentence assigning a value to a given variable.
		Inputs:
		-> A sentence to simplify.

		Returns:
		-> Simplified sentence."""

	deleteList = []
	# Run through clauses in sentence
	for clause in sentence:

		# Run through symbols in clause
		for symbol in clause:
		# If symbol is not variable, set to False
			if -symbol == variable:
				#sentence[i][j] = False
				clause.remove(symbol)

			# If symbol is variable, set to True
			elif symbol == variable:
				#sentence[i][j] = True
				deleteList.append(clause)
	
	# Delete clauses with True
	for dl in deleteList:
		sentence.remove(dl)
	#sentence = filter(None,sentence)
	return sentence

def DPLL_old(sentence,sol=None,i=None):
	""" DPLL Algorithm Implementation"""
	
	# Initialize solution vector and iteration counter
	if sol == None or i == None:
		sol = []
		i = 0
	i+=1

	# Checks if there are only empty lists inside sentence, which mean that we had a False in the conjunction or a True statement
	if len(filter(None,filter(None,sentence))) == 0:
		
		# If sentence isn't empty, then we had empty lists inside our sentence, which mean we had a False statement in the conjunction: it can't be satisfied
		if len(sentence) != 0:
			return [],False, i
		return sol, True, i
	
	# Check if sentence has pure symbols
	symbol, position = hasPureSymbol(sentence)
	if symbol != None:
		simplSymbol = symbol
 	
 	# Check if sentence has unit clauses
	cindex, value = hasUnitClause(sentence)
	if cindex != None:	
		simplSymbol = value[0]

	# If it doesn't have pure symbols or unit clauses, grab first variable in sentence
	if symbol == None and cindex == None:
		simplSymbol = sentence[0][0]

	# Get simplified sentence in order to the symbol we want to assign
	simple_sentence = simplifySentence(sentence,simplSymbol)
	
	# Append assignment to solution list
	sol.append(simplSymbol)

	result = DPLL(simple_sentence,sol,i)
	return result

def AllTrue(sentence, model):

	for clause in sentence:
		val = []	
		for symbol in clause:
			if(abs(symbol) in model):
				if(symbol>0):
					val.append(model[abs(symbol)])
				else:
					val.append(not model[abs(symbol)])
		if(val == [] or not True in val):
			return False

	return True

def AnyFalse(sentence,model):

	for clause in sentence:
		val = []	
		valid = True
		for symbol in clause:
			if(abs(symbol) in model):
				if(symbol>0):
					val.append(model[abs(symbol)])
				else:
					val.append(not model[abs(symbol)])
			else:
				valid = False
		if(not True in val and val != [] and valid):
			return True

	return False
def DPLL(S,symbols,model = {}):
	#print "IN->",model
	if AllTrue(S,model): 
		# print "TRUE!!!!" , model
		return model,True

	if AnyFalse(S,model): 
		#print "FALSE"
		return model,False

	# Check if pure-symbol exists
	P,value = hasPureSymbol(S,symbols,model)
	if P != None:
		symbols.remove(P)
		model[P] = value
		return DPLL(S,symbols,model)

	# Check has unit clauses
	P,value = hasUnitClause(S,symbols,model)
	if P != None:
		symbols.remove(P)
		model[P] = value
		return DPLL(S,symbols,model)

	P = symbols[0]
	rest = symbols[1:]
	
	model1 = copy.deepcopy(model)
	model1[P] = True
	modelOut,value1 = DPLL(S,rest,model1)
	if value1 == True: model = modelOut
	
	model2 = copy.deepcopy(model)
	model2[P] = False
	modelOut,value2 = DPLL(S,rest,model2)
	if value2 == True: model = modelOut
	#print "OUT->", model
	return model,(value1 or value2)

def transformOut(model,symbols):
	finalmodel = []
	for i in symbols:
		if i in model:
			finalmodel.append(model[i])
		else:
			finalmodel.append(False)
	return finalmodel

def uniTestDPLL():
	# TEST zone below
#	logging.basicConfig(stream=sys.stderr, level=logging.ERROR)
	stype, symbols, clauses, sentence = parser("Tests/uf20-01000.cnf")

	#sentence.append([-7,-4])
	#sentence.append([4])
	#sentence = [[-1, -2], [1]]
	#print symbol
	#print sentence
	symbol = range(1,symbols+1)
	#print hasUnitClause(sentence)
	val , model = DPLL(sentence,symbol)
	print("Model->",model.values())
	finalmodel = []
	for i in range(1,symbols+1):
		if i in model:
			finalmodel.append(model[i])
		else:
			finalmodel.append(False)
	#print finalmodel
	print(util.evaluate(sentence,finalmodel))
	#sentence = [[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1]]


