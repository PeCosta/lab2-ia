%function get_fit();
clear all
fid = fopen('ParsedData/DPLL-S.csv');
DPLLS = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/GSAT-S.csv');
GSATS = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/WalkSAT-S.csv');
WalkSATS = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/GSAT-NS.csv');
GSATNS = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/WalkSAT-NS.csv');
WalkSATNS = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/GSAT-L.csv');
GSATL = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/WalkSAT-L.csv');
WalkSATL = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/DPLL-NT.csv');
DPLLNT = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/WalkSAT-NT.csv');
WalkSATNT = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

fid = fopen('ParsedData/GSAT-NT.csv');
GSATNT = textscan(fid,'%f%f%f%f%s','delimiter',',');
fclose(fid);

% Calc Media em cada ponto
%
% Type define o que queremos analisar:
% 1 - Variaveis;
% 2 - Clauses;
% 3 - Racio;
% Quando se altera o type, tem que se alterar no csvwrite o caminho onde se
% guarda o resultado.
%
% Para analisar os vários algoritmos, basta escolher da lista abaixo e
% substituir nas linhas 57,58 e no csvwrite, linha 91.
% DPLLS DPLLNT WalkSATS WalkSATNS WalkSATNT WalkSATL GSATS GSATL GSATNS
% GSATNT
TYPE = 3;
x(:,1) = GSATNT{TYPE};
x(:,2) = GSATNT{4};
x = sortrows(x,1);
x(:,3) = zeros(size(x,1),1);

a = unique(x(:,1));
k=1;
j=1;
x(1,3) = x(1,2);
for i=2:size(x,1)
    if x(i,1) == x(i-1,1)
        x(i,3) = x(i-1,3)+x(i,2);
        j=j+1;
    else
        a(k,2) = x(i-1,3)/j;
        j = 1;
        k=k+1;
        x(i,3) = x(i,2);
    end
end
a(k,2) = x(i,3)/j;
if(size(a,1)>1)
    figure(1);
    plot(a(:,1),a(:,2))
    if TYPE == 1
        xlabel('Number of Variables');
    elseif TYPE == 2
        xlabel('Number of Clauses');
    elseif TYPE == 3
        xlabel('C/N Ratio');
    end
    ylabel('Average Time until Solution');
else
    a
    csvwrite('Results/Ratio/GSATNT',a)
end