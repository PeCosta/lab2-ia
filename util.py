import sys
import logging
from random import getrandbits

def evaluate(S,test):
	logging.debug("--Evaluate--")
	count = len(S)
	for clause in S:
		val = []
		for param in clause:
			if param > 0:
				val.append(test[param-1])
			else:
				val.append(not test[abs(param)-1])
		#print val
		if (True not in val):
			count = count-1
	logging.debug("Count->"+str(count))
	if(count==len(S)):
		return True,count
	else:
		return False,count

def RandomTest(n):
	result = []
	for i in range(0,n):
		result.append(bool(getrandbits(1)))
	return result
