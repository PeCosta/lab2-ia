import sys
from random import getrandbits, choice, uniform
import logging
from util import evaluate, RandomTest
from copy import deepcopy


def getRandomFalseClause(S,test):
	"""
	Get a random clause from does that compute false
	S -> Sentence with Clauses
	test -> Solution to test
	"""
	#Variable to store possible clauses
	possible = []

	#Iterate Clauses
	for clause in S:
		val = []

		#Iterate variables in clauses
		for param in clause:
			#Test Logic
			if param > 0:
				val.append(test[param-1])
			else:
				val.append(not test[abs(param)-1])
		#If no True value in clause then clause computes false
		if (True not in val):
			#Store false computed clause
			possible.append(clause)

	#return a random clause form the possible ones
	return choice(possible)

def flip(S,A,clause):
	"""
	Flip the variable that presents the best result in Sentence
	S -> Sentence with Clauses
	A -> Solution to walk from
	clause -> Clause to flip a variable from
	"""

	#Weighted Flip it is
	countF = -1

	#Iterate Variables in the Clause
	for variable in clause:

		#copy A to B
		B = deepcopy(A)

		#Flip variable in B
		B[abs(variable)-1] = not B[abs(variable)-1]

		#Evaluate B
		finish,count = evaluate(S,B)
		if(count > countF):
			countF = count
			sol = variable

	#Flip Best Variable in A
	A[abs(sol)-1] = not A[abs(sol)-1]  

	#Log in info layer
	logging.info("A->"+str(A))	

	#Return New Possible Solution
	return A

def randomFlip(A,clause):
	"""
	Randomly Flip a variable in clauses
	A -> Solution to walk from
	clause -> Clause to flip a variable from
	"""

	#Select a random variable in clause and flip it
	var = choice(clause)
	A[abs(var)-1] = not A[abs(var)-1]

	#Log in info layer
	logging.info("A->"+str(A))

	#Return new possible solution
	return A

def walk(S,A,p):
	"""
	Walk to next possible Solution
	S -> Sentence with Clauses
	A -> Solution to walk from
	p -> Probability of preforming a random walk
	"""
	#Get a random clause that computes false 
	clause  = getRandomFalseClause(S,A)

	#Randomly select a randomFlip or a weighted Flip
	if uniform(0,1)>p:
		A = flip(S,A,clause)
	else:
		A = randomFlip(A,clause)

	#Return new possible Solution
	return A


def WalkSat(S,p,max_flips,n):
	"""
		Implements WalkSat Algorithm
		Inputs:
		S -> Sentence with Clauses (len(S) represents number of clauses)
		p -> Probability of preforming a random walk
		max_flips -> Maximum number of permitted flips to the variables 
	"""
	A = RandomTest(n)
	#Walk until you are tired
	for i in range(0,max_flips):
		logging.info("Flip Number ->"+str(i))
		logging.info("A->" + str(A))
		finish,count = evaluate(S,A)
		#Is the problem Solved?
		if(finish == True):
			#You got yourself a solution, guess that walking was worth it!
			logging.info("Solution Found -> "+str(A))
			return A , True,i
				
		#Guess not (Lets Walk a bit further)
		logging.info("Solution Not Found, Lets Walk")
		A = walk(S,A,p)
		logging.info("A->"+str(A))
	logging.info("No Solution, Maybe walk a little bit more")
	return [],False, i

#def unitTestWalkSat():
#	Type,n,N,S = parser("Tests/uf20-01.cnf")
#
#	A = RandomTest(n)
#	WalkSat(S,p=0.5,max_flips=1000,n=n)
#	return

#unitTestWalkSat()